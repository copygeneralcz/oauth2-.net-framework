﻿using System;
using System.IdentityModel.Tokens;
using System.Threading.Tasks;
using IdentityServer3.AccessTokenValidation;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(WebApplication.App_Start.Startup))]

namespace WebApplication.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            JwtSecurityTokenHandler.InboundClaimTypeMap.Clear();
            app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions()
            {
                Authority = "https://localhost:44350",
                ClientId = "api1",
                ClientSecret = "ScopeSecret",
                RequiredScopes = new[] { "api1.read" },
                ValidationMode = ValidationMode.ValidationEndpoint
            });
        }
    }
}
