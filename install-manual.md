***Instalovat ověřování pomocí OAuth2 token***

Ukázkový příklad jak nainstalovat doplňky pro ověřování pomocí OAuth2 tokenů

## Vytvořit aplikaci novou aplikaci
- ASP.NET Web Application (.NET Framework)
![](Install-manual-app_1.png)
- Web API (MVC a Web API)
![](Install-manual-app_2.png)

## Nainstalovat tyto Nuget package
- Install-Package Owin
- Install-Package Microsoft.Owin
- Install-Package Microsoft.Owin.Host.SystemWeb
- Install-Package IdentityServer3.AccessTokenValidation


## Ve složce App_Start vytvořit soubor Startup.cs
![](Install-manual-app_3.png)
- Použít *OWIN Startup class*
![](Install-manual-app_4.png)


## Do sekce

```sh
public void Configuration(IAppBuilder app)
{

}
```

doplnit následující kód

```sh
JwtSecurityTokenHandler.InboundClaimTypeMap.Clear();
app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions()
{
    Authority = "https://localhost:44350",
    ClientId = "api1",
    ClientSecret = "ScopeSecret",
    RequiredScopes = new[] { "api1.read" },
    ValidationMode = ValidationMode.ValidationEndpoint
});
```

Teď už stačí aplikovat
`[Authorize]`
do API interface.


# Jak to funguje
- Aplikace přijme bearer token
- poté pošle ověřovací request na `Authority server` a na endpoint `/connect/introspect`
- pokud je úspěšně ověřen vrací OK a aplikace nasetuje proměnou `User`

```sh
curl --location --request POST 'https://localhost:44350/connect/introspect' \
--header 'scope: default openid' \
--header 'Authorization: Basic YXBpMTpTY29wZVNlY3JldA==' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'token=eyJ ..... _6A'
```


Testováno podle tohoto příkladu:
https://www.happycoder.gr/blog/securing-net-framework-45-with-identity-server-4/